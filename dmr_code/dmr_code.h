#ifndef DATA_H
#define DATA_H

class data
{
    char *tydzien[7]={"sobota","niedziela","poniedzialek","wtorek","sroda","czwartek","piatek"};
    int dni_mies[12]={31,28,31,30,31,30,31,31,30,31,30,31};
    int dni_mies_rok[12];
public:
    data();
    ~data();
    bool sprawdz_date(int d, int m, int r);
    bool rok_przestepny(int r);
    int ile_przestepnych(int r);
};

#endif // DATA_H
