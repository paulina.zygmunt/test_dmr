#include "dmr_code.h"

data::data(){
}

data::~data(){
}

bool data::sprawdz_date(int d, int m, int r) //1 - istnieje, 0 - nie istnieje
{
//  return (d>0 && d<=dni_mies[m-1]) && (m>=1 && m<=12) && (r>=1582 && r<=2999); //kalendarz gregorianski
  return (d>0 && d<=dni_mies[m-1]) && (m>=1 && m<=12) && (r>=0 && r<=2999);
}

bool data::rok_przestepny(int r) //1 - tak, 0 - nie
{
  return (r%400==0 || (r%4==0 && r%100!=0));
}

int data::ile_przestepnych(int r) //suma lat przestepnych od roku 0 wlacznie
{
  int n,iprzestepnych=0;
  for(n=1;n<r;n++) {
    if(rok_przestepny(n)) iprzestepnych++;
  }
  return iprzestepnych;
}
