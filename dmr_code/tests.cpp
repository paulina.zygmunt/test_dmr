#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "dmr_code.h"

using namespace testing;

// podejœcie dla prostych testów bez wspó³dzielenia zasobów takich jak obiekt klasy testowanej
TEST(proba_daty, sprawdz_date__wej_32_10_2000__wyj_0)
{
    // ARRANGE:
    data obiekt_do_testow;
    int d = 32;
    int m = 10;
    int r = 2000;
    int result = 0;

    // ACT:
    int act = obiekt_do_testow.sprawdz_date(d, m, r);

    // ASSERT:
    EXPECT_EQ(act, result);
}

TEST(proba_daty, sprawdz_date__wej_31_10_2000__wyj_1)
{
    // ARRANGE:
    data obiekt_do_testow;
    int d = 31;
    int m = 10;
    int r = 2000;
    int result = 1;

    // ACT:
    int act = obiekt_do_testow.sprawdz_date(d, m, r);

    // ASSERT:
    EXPECT_EQ(act, result);
}

//

/// podejœcie z klas¹ testow¹ dla przypadków gdy testujemy ró¿ne przypadki tej samej jednostki testowe np.: metody

class sprawdzDate : public testing::Test
{
public:
    // ARRANGE:
    data obiekt_do_testow;
    int d, m, r;
    int result;

    virtual void SetUp() // metoda uruchamiana niejawnie na pocz¹tku testu
    {
        // kod uruchamiany po wejœciu do testu
    }
    virtual void TearDown() // metoda uruchamiana niejawnie na koñcu testu
    {
        // jakiœ kod uruchamiany po wykonaniu testu
    }
};


TEST_F(sprawdzDate, sprawdz_date__wej_31_10_2000__wyj_0)
{
    // ARRANGE:
    d = 31;
    m = 10;
    r = 2000;
    result = 1;

    // ACT:
    int act = obiekt_do_testow.sprawdz_date(d, m, r);

    // ASSERT:
    EXPECT_EQ(act, result);
}

TEST_F(sprawdzDate, sprawdz_date__wej_32_10_2000__wyj_0)
{
    // ARRANGE:
    d = 32;
    m = 10;
    r = 2000;
    result = 0;

    // ACT:
    int act = obiekt_do_testow.sprawdz_date(d, m, r);

    // ASSERT:
    EXPECT_EQ(act, result);
}


class rokPrzestepnyTest : public testing::Test
{
public:
    data obiekt_do_testow;
    int r;
    int result;
};

TEST_F(rokPrzestepnyTest, czy_przestepny__wej_2000__wyj_1)
{
    // ARRANGE:
    r = 2000;
    result = 1;

    // ACT:
    int act = obiekt_do_testow.rok_przestepny(r);

    // ASSERT:
    EXPECT_EQ(act, result);
}

TEST_F(rokPrzestepnyTest, czy_przestepny__wej_2001__wyj_0)
{
    // ARRANGE:
    r = 2001;
    result = 0;

    // ACT:
    int act = obiekt_do_testow.rok_przestepny(r);

    // ASSERT:
    EXPECT_EQ(act, result);
}


class ilePrzestepnychTest : public testing::Test
{
public:
    // ARRANGE:
    data obiekt_do_testow;
    int r;
    int result;
};

TEST_F(ilePrzestepnychTest, ile_przestepnych__wej_10__wyj_2)  //suma lat przestepnych od roku 0 n.e.
{
    // ARRANGE:
    r = 10;
    result = 2;

    // ACT:
    int act = obiekt_do_testow.ile_przestepnych(r);

    // ASSERT:
    EXPECT_EQ(act, result);
}

TEST_F(ilePrzestepnychTest, ile_przestepnych__wej_minus5__wyj_0)  //suma lat przestepnych od roku 0 n.e.
{
    // ARRANGE:
    r = -5;
    result = 0;

    // ACT:
    int act = obiekt_do_testow.ile_przestepnych(r);

    // ASSERT:
    EXPECT_EQ(act, result);
}

TEST_F(ilePrzestepnychTest, ile_przestepnych__wej_0__wyj_0)  //suma lat przestepnych od roku 0 n.e.
{
    // ARRANGE:
    r = 0;
    result = 0;

    // ACT:
    int act = obiekt_do_testow.ile_przestepnych(r);

    // ASSERT:
    EXPECT_EQ(act, result);
}
