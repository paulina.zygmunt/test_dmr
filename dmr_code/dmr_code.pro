include(gtest_dependency.pri)

TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG += thread
CONFIG -= qt

HEADERS += \
        dmr_code.h \
        tests.cpp

SOURCES += \
        dmr_code.cpp \
        main.cpp
